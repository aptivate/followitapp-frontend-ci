 [![pipeline status](https://git.coop/aptivate/followitapp-frontend-ci/badges/master/pipeline.svg)](https://git.coop/aptivate/followitapp-frontend-ci/commits/master)

# followitapp-frontend-ci

A container to speed up builds for [followitapp-mobile-docker].

[followitapp-mobile-docker]: https://git.coop/aptivate/followitapp-mobile-docker/

This image is hosted on [Docker Hub] at the following URL:

[Docker Hub]: https://hub.docker.com/

> https://hub.docker.com/r/aptivate/followitapp-frontend-ci/

## Install Docker

It's really easy with the cross platform installation script:

```bash
$ curl -sSL https://get.docker.com/ | sh
```

## Docker Hub Access

See the pass store for access details.

## How We Push Images To Docker Hub

The Docker hub login details are stored in encrypted variables in the repository settings.

Please see the `.gitlab-ci.yml` for the CI process and how it gets uploaded.

Each build of this repository pushes a new image to the Docker hub.

## Hack On It

We use the usual `Makefile` convenience targets of:

  * `make docker-build`: build the image
  * `make docker-push`: push the image to Docker hub

You'll need to run `docker login` on your local before pushing.
