IMAGE="aptivate/followitapp-frontend-ci"

docker-build:
	@docker build $(DOCKER_ARGS) -t $(IMAGE) .
.PHONY: docker-build

docker-push:
	@docker push $(IMAGE)
.PHONY: docker-push
