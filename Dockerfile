FROM thyrlian/android-sdk

MAINTAINER MediaCoop Team <mediacoop-team@aptivate.org>

RUN mkdir -p $HOME/.android/
RUN touch $HOME/.android/repositories.cfg

RUN apt-get update -q && apt-get install -yq curl wget
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN apt-get update -q && apt-get install -yqq gcc g++ make nodejs

RUN dpkg --add-architecture i386 && \
  apt-get update -q && \
  apt-get install -yq \
    gcc \
    g++ \
    libegl1-mesa \
    libgcc1:i386 \
    libncurses5:i386 \
    libstdc++6:i386 \
    libvirt0 \
    libvirt-dev \
    make \
    module-init-tools \
    nodejs \
    qemu \
    qemu-kvm \
    zlib1g:i386

RUN npm i -g npm@latest
RUN npm i -g react-native-cli

RUN yes | sdkmanager --licenses && sdkmanager --verbose \
  "system-images;android-29;google_apis;x86" \
  "build-tools;29.0.0" \
  "platforms;android-29" \
  "platform-tools" \
  "emulator"
